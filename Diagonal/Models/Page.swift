//
//  Page.swift
//  Diagonal
//
//  Created by Lal Krishna on 14/07/19.
//  Copyright © 2019 Ravanar. All rights reserved.
//

import Foundation

// MARK: - Page

struct Page: Codable {
    let title, totalContentItems, pageNum, pageSize: String
    let contentItems: ContentItems
    
    enum CodingKeys: String, CodingKey {
        case title
        case totalContentItems = "total-content-items"
        case pageNum = "page-num"
        case pageSize = "page-size"
        case contentItems = "content-items"
    }
}
