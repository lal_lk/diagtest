//
//  Content.swift
//  Diagonal
//
//  Created by Lal Krishna on 14/07/19.
//  Copyright © 2019 Ravanar. All rights reserved.
//

import Foundation

// MARK: - Content

struct Content: Codable {
    let name: String
    let posterImage: String?
    
    enum CodingKeys: String, CodingKey {
        case name
        case posterImage = "poster-image"
    }
}
