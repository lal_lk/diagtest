//
//  Response.swift
//  Diagonal
//
//  Created by Lal Krishna on 14/07/19.
//  Copyright © 2019 Ravanar. All rights reserved.
//

import Foundation

// MARK: - Response

struct Response: Codable {
    let page: Page
}
