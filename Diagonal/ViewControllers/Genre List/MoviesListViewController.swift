//
//  MoviesListViewController.swift
//  Diagonal
//
//  Created by Lal Krishna on 14/07/19.
//  Copyright © 2019 Ravanar. All rights reserved.
//

import UIKit

class MoviesListViewController: UIViewController {
    
    var contents = [Content]()
    var currentPageNumber = 1
    var totalContentItems: Int = 0
    var isLoading = false
    
    let columnCount: CGFloat = 3
    let columnCountLandscape: CGFloat = 5
    
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var moviesCollectionView: UICollectionView! {
        didSet {
            moviesCollectionView.delegate = self
            moviesCollectionView.dataSource = self
            moviesCollectionView.prefetchDataSource = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        moviesCollectionView.contentInset = UIEdgeInsets(top: navigationView.bounds.height, left: 10, bottom: 30, right: 10)
        
        fetchData()
    }
    
    // MARK: Fetch Data
    
    func fetchData() {
        
        MovieFetcher.fetchRomanticComedy(pageNumber: currentPageNumber) { [weak self] page in
            guard let page = page else { return }
            self?.totalContentItems = Int(page.totalContentItems)!
            self?.contents.append(contentsOf: page.contentItems.content)
            DispatchQueue.main.async {
                self?.moviesCollectionView.reloadData()
            }
            
        }
    }
    
    
    func loadMoreRomanticComedy() {
        isLoading = true
        currentPageNumber += 1
        MovieFetcher.fetchRomanticComedy(pageNumber: currentPageNumber) { [weak self] page in
            
            guard let page = page, page.contentItems.content.count > 0 else {
                self?.isLoading = false
                return                
            }
            self?.totalContentItems = Int(page.totalContentItems)!
            
            self?.moviesCollectionView.performBatchUpdates({
                if let indexPaths = self?.indexPathsToReload(currentCount: self?.contents.count ?? 0, newCount: page.contentItems.content.count) {
                    self?.contents.append(contentsOf: page.contentItems.content)
                    self?.moviesCollectionView.insertItems(at: indexPaths)
                }
            }, completion: { finished in
                self?.isLoading = false
            })
            
        }
    }
    
    private func indexPathsToReload(currentCount: Int, newCount: Int) -> [IndexPath] {
        let indexPaths = (currentCount..<currentCount + newCount).map { IndexPath(item: $0, section: 0) }
        return indexPaths
    }
    
    // MARK: Actions
    
    @IBAction func didTapBackButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    
}

// MARK: - UICollectionView Delegate, DataSource

extension MoviesListViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contents.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieCollectionViewCell.identifier, for: indexPath) as! MovieCollectionViewCell
        if indexPath.row < contents.count {
            let content = contents[indexPath.row]
            let posterImage: UIImage? = (content.posterImage != nil) ? UIImage(named: content.posterImage!) : nil
            cell.configure("\(content.name) \(indexPath.row)", image: posterImage)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        //
        if let cell = cell as? MovieCollectionViewCell, indexPath.row < contents.count {
            let content = contents[indexPath.row]
            let posterImage: UIImage? = (content.posterImage != nil) ? UIImage(named: content.posterImage!) : nil
            cell.configure("\(content.name) \(indexPath.row)", image: posterImage)
        }
        
        if indexPath.row == contents.count - 1, contents.count < totalContentItems, !isLoading {
            loadMoreRomanticComedy()
        }

    }
}

// MARK: - UICollectionViewDataSourcePrefetching

extension MoviesListViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        guard (indexPaths.filter { $0.item == contents.count - 1 }).count > 0, contents.count < totalContentItems, !isLoading else { return }
        loadMoreRomanticComedy()
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout

extension MoviesListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let columnCount = (collectionView.bounds.width > collectionView.bounds.height) ? self.columnCountLandscape : self.columnCount

        var collectionViewWidth = collectionView.bounds.width - collectionView.contentInset.left - collectionView.contentInset.right
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            collectionViewWidth -= layout.minimumInteritemSpacing * (columnCount - 1)
        }
        let width = (collectionViewWidth / columnCount).rounded(.towardZero)
        let height = ((217 / 128) * width).rounded(.towardZero)
        return CGSize(width: width, height: height)
    }
    
}

