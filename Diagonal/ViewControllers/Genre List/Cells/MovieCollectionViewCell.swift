//
//  MovieCollectionViewCell.swift
//  Diagonal
//
//  Created by Lal Krishna on 14/07/19.
//  Copyright © 2019 Ravanar. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    
    static let identifier = String(describing: MovieCollectionViewCell.self)
    
    override func prepareForReuse() {
        super.prepareForReuse()
        posterImageView.image = UIImage(named: "placeholder_for_missing_posters")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func configure(_ title: String, image: UIImage?) {
        self.posterImageView.image = image ?? UIImage(named: "placeholder_for_missing_posters")
        self.movieTitleLabel.text = title
    }
    
}
