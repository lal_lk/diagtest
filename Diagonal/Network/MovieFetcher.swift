//
//  MovieFetcher.swift
//  Diagonal
//
//  Created by Lal Krishna on 14/07/19.
//  Copyright © 2019 Ravanar. All rights reserved.
//

import Foundation

struct MovieFetcher {
    
    static func fetchRomanticComedy(pageNumber: Int, completionHandler: @escaping (_ page: Page?)->Void) {
        let fileName = "CONTENTLISTINGPAGE-PAGE\(pageNumber)"
        DispatchQueue.global(qos: .background).async {
            guard let path = Bundle.main.path(forResource: fileName, ofType: "json") else {
                DispatchQueue.main.async {
                    completionHandler(nil)                    
                }
                return
            }
            do {
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let decoder = JSONDecoder()
                let response = try decoder.decode(Response.self, from: jsonData)
                DispatchQueue.main.async {
                    completionHandler(response.page)
                }
            } catch {
                DispatchQueue.main.async {
                    completionHandler(nil)
                }
            }
        }
        
    }
    
}
