//
//  DiagonalTests.swift
//  DiagonalTests
//
//  Created by Lal Krishna on 14/07/19.
//  Copyright © 2019 Ravanar. All rights reserved.
//

import XCTest
@testable import Diagonal

enum DiagonalError: Error {
    case message(_ message: String)
}

class DiagonalTests: XCTestCase {

    override func setUp() {
        
    }

    override func tearDown() {
        
    }

    func testJSONDecoding() {
        for pageNumber in 1...3 {
           
            do {
                let _ = try fetchResponse(for: pageNumber)
            } catch {
                let error = error as! DiagonalError
                if case let DiagonalError.message(message) = error {
                    XCTFail(message)
                }
                XCTFail("Error")
            }
        }
    }
    
    
    func testApisCount() {
        do {
            let firstPage = try fetchResponse(for: 1).page
            let secondPage = try fetchResponse(for: 2).page
            let thirdPage = try fetchResponse(for: 3).page
            
            let totalItemCount = Int(firstPage.totalContentItems)!
            
            let firstPageItemsCount = firstPage.contentItems.content.count
            let secondPageItemsCount = secondPage.contentItems.content.count
            let thirdPageItemsCount = thirdPage.contentItems.content.count
            
            XCTAssertEqual(firstPageItemsCount + secondPageItemsCount + thirdPageItemsCount, totalItemCount, "Total Item Count Mismatch")
            
        } catch {
            if case let DiagonalError.message(message) = error {
                XCTFail(message)
            }
        }
    }
    

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func fetchResponse(for pageNumber: Int) throws -> Response {
         let fileName = "CONTENTLISTINGPAGE-PAGE\(pageNumber)"
        guard let path = Bundle.main.path(forResource: fileName, ofType: "json") else {
            throw DiagonalError.message("Resource File Not Found, named: \(fileName)")
        }
        do {
            let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let decoder = JSONDecoder()
            let response = try decoder.decode(Response.self, from: jsonData)
            return response
        } catch {
            throw DiagonalError.message("Decode error")
        }
    }

}
